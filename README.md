# @ki2/events

Event manager able to handle event defined by a string name.

## Usage

```ts
import { EventStore } from "@ki2/events";

// define events manager
const events = new EventStore();

// define callback function
function myCallback() {
  console.log("Hello Event !");
}

// Register callback function on my-custom-event event
events.on("my-custom-event", myCallback);
// or events.addEventListener("my-custom-event", myCallback)

// Emit event
events.emit("my-custom-event"); // --> Hello Event !
```
