export type EventCallBackFunctionType = (data?: any) => void | Promise<void>;

type EventCB = EventCallBackFunctionType;

export class EventHandler {
  private callbacks: Array<EventCallBackFunctionType> = [];

  get length(): number {
    return this.callbacks.length;
  }

  dispose() {
    this.callbacks = [];
  }

  indexOf(cb: EventCB): number {
    return this.callbacks.indexOf(cb);
  }

  exist(cb: EventCB): boolean {
    return this.indexOf(cb) > -1;
  }

  add(cb: EventCB): EventHandler {
    if (!this.exist(cb)) {
      this.callbacks.push(cb);
    } else {
      console.warn(
        "[EventHandler] Trying to add existing callback twice: ignoring"
      );
    }
    return this;
  }

  remove(cb: EventCB): EventHandler {
    if (this.exist(cb)) {
      const index = this.indexOf(cb);
      this.callbacks.splice(index, 1);
    } else {
      console.warn(
        "[EventHandler] Trying to remove non-existing callback function: ignoring"
      );
    }
    return this;
  }

  emit(data?: any): EventHandler {
    for (const cb of this.callbacks) {
      cb(data);
    }
    return this;
  }

  async asyncEmit(data?: any): Promise<EventHandler> {
    let arr: Array<Promise<unknown>> = [];
    for (const cb of this.callbacks) {
      let p = cb(data);
      if (p instanceof Promise) {
        arr.push(p);
      }
    }
    await Promise.all(arr);
    return this;
  }
}
