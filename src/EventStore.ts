import { EventHandler, EventCallBackFunctionType } from ".";

export class EventStore {
  eventhandlers: Map<string, EventHandler> = new Map<string, EventHandler>();

  dispose() {
    this.eventhandlers.forEach((handler) => handler.dispose());
  }

  private has(event: string) {
    return this.eventhandlers.has(event);
  }

  private get(event: string) {
    return this.eventhandlers.get(event);
  }

  private set(event: string, handler: EventHandler): void {
    this.eventhandlers.set(event, handler);
  }

  private remove(event: string) {
    this.eventhandlers.delete(event);
  }

  private getHandler(event: string): EventHandler {
    if (this.has(event)) {
      return this.get(event)!;
    }
    const newhandler = new EventHandler();
    this.set(event, newhandler);
    return newhandler;
  }

  /** addEventListener
   *
   * Alias of `on`
   *
   * @param event event name
   * @param cb callback function (triggered when the event is emited)
   * @returns this
   */
  addEventListener(event: string, cb: EventCallBackFunctionType): EventStore {
    this.getHandler(event).add(cb);
    return this;
  }

  /** on
   *
   * Alias of `addEventListener`
   *
   * @param event event name
   * @param cb callback function (triggered when the event is emited)
   * @returns this
   */
  on(event: string, cb: EventCallBackFunctionType): EventStore {
    this.getHandler(event).add(cb);
    return this;
  }

  /** removeEventListener
   *
   * @param event event name
   * @param cb callback function to remove
   * @returns this
   */
  removeEventListener(
    event: string,
    cb: EventCallBackFunctionType
  ): EventStore {
    if (!this.has(event)) {
      return this;
    }
    const handler = this.getHandler(event).remove(cb);
    if (handler.length <= 0) {
      this.remove(event);
    }
    return this;
  }

  /** emit
   *
   * Emit an event and pass data to callback functions.
   *
   * Is some callback function may be asynchronous and
   * you need to await them, use asyncEmit instead.
   *
   * @param event
   * @param data
   * @returns
   */
  emit(event: string, data?: any): EventStore {
    if (this.has(event)) {
      this.get(event)!.emit(data);
    }
    return this;
  }

  /** asyncEmit
   *
   * Emit an event and pass data to callback functions.
   * If there is some async callback, the full emit is
   * awaitable.
   *
   * @param event
   * @param data
   * @returns
   */
  async asyncEmit(event: string, data?: any): Promise<EventStore> {
    if (this.has(event)) {
      await this.get(event)!.asyncEmit(data);
    }
    return this;
  }
}
