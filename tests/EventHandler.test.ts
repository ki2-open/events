import { EventHandler } from "../src";

describe("test event handler", () => {
  it("should correclty add & remove callbacks", () => {
    const f1 = () => {
      return;
    };

    const eh = new EventHandler();
    expect(eh.length).toBe(0);
    eh.add(f1);
    expect(eh.length).toBe(1);
    expect(eh.indexOf(f1)).toBeGreaterThanOrEqual(0);
    expect(eh.exist(f1)).toBe(true);

    const f2 = () => {
      return;
    };

    eh.add(f2);
    expect(eh.length).toBe(2);
    expect(eh.indexOf(f2)).toBeGreaterThanOrEqual(0);
    expect(eh.exist(f2)).toBe(true);

    eh.remove(f1);
    expect(eh.length).toBe(1);
    expect(eh.exist(f1)).toBe(false);
    expect(eh.exist(f2)).toBe(true);

    eh.remove(f2);
    expect(eh.length).toBe(0);
    expect(eh.exist(f2)).toBe(false);
    expect(eh.exist(f2)).toBe(false);
  });
});
